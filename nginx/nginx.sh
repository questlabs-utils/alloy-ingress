#!/bin/bash

# Prompt the user for the new namespace
echo "Enter the new namespace for Ingress resources:"
read new_namespace

# Fetch the file, replace the placeholder, and apply the configuration
curl -k https://gitlab.com/questlabs-utils/alloy-ingress/-/raw/main/nginx/customnginx.yaml > nginx.yaml && sed -i "s/ingressnamespace/$new_namespace/" nginx.yaml && kubectl apply -f nginx.yaml
